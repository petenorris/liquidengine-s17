<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	

	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'liquidengine' ); ?></a>

	<header class="site-header">
		<?php get_template_part('template-parts/navbar/navbar' ); ?>
		<div class="site-branding">
			<div class="container">
				<?php if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php endif; ?>
			</div>
		</div>

		<?php if ( is_front_page() or is_home() ) : ?>
			<?php // get_template_part('template-parts/elements/slider' ); ?>
		<?php else : ?>
			<?php // get_template_part('template-parts/elements/banner', 'featured' ); ?>
		<?php endif; ?>
		<?php // SIDR menu is now in footer to hide display flash ?>
	</header>

	<div id="content" class="site-content">
