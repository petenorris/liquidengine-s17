<?php

//////////////////
//////////////////
////  //      ////
////  ////////////
////  //      ////
////  ////////////
////          ////
//////////////////
//////////////////

// Copyright Liquid Engine Ltd - https://liquidengine.com 


/*
Template Name: Scroll Magic
*/


get_header(); ?>

<header id="header">
	<h1>this is the header</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam quidem dignissimos alias illum quas asperiores, laudantium nobis est minima, unde fuga aliquid, quasi voluptatum, tenetur perspiciatis rem vero aliquam sequi.</p>
	<ul class="list">
		<li>lista</li>
		<li>listb</li>
		<li>listc</li>
	</ul>
</header>

<!-- Parallax -->
<div class="parallax">
	<div class="parallax-bg"></div>
	<div class="parallax-content">
		<h1>Section with Parallax</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut mollitia explicabo, quasi, officiis dicta similique reprehenderit modi hic, voluptates officia sint voluptate ut inventore maxime. Reiciendis fugit debitis eius, adipisci!</p>
	</div>
</div>

<!-- Features -->
<div class="card featured">
	<div class="featured-item">
		<img src="<?php bloginfo('template_directory') ?>/assets/images/logo.svg" alt="">
	</div>
	<div class="featured-item">
		<img src="<?php bloginfo('template_directory') ?>/assets/images/logo.svg" alt="">
	</div>
	<div class="featured-item">
		<img src="<?php bloginfo('template_directory') ?>/assets/images/logo.svg" alt="">
	</div>
	<div class="featured-item">
		<img src="<?php bloginfo('template_directory') ?>/assets/images/logo.svg" alt="">
	</div>
</div>




<div class="card-intro">
	<div class="introcontent">
		<h1>Howdi!</h1>	
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores illum optio, esse voluptas fugiat, sint! Suscipit voluptates corrupti quod delectus vero reprehenderit minima ipsum omnis, nobis est ad, fugit odit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae ex optio quas obcaecati in, cum, eos. Asperiores quam sed iusto modi, tempori</p>
	</div>
</div>



<div class="scroll-fade-in">
	<div class="card-scroll">
		hello
	</div>
	<div class="card-scroll">
		Welcome	
	</div>
	<div class="card-scroll">
		you
	</div>
</div>





<div class="spacer"></div>

<?php get_footer();
