<?php
//////////////////
//////////////////
////  //      ////
////  ////////////
////  //      ////
////  ////////////
////          ////
//////////////////
//////////////////

// Copyright Liquid Engine Ltd - https://liquidengine.com 

if ( ! is_active_sidebar( 'footer-widgets' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'footer-widgets' ); ?>
</aside><!-- #secondary -->