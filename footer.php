<?php
//////////////////
//////////////////
////  //      ////
////  ////////////
////  //      ////
////  ////////////
////          ////
//////////////////
//////////////////

// Copyright Liquid Engine Ltd - https://liquidengine.com 
?>

</div><!-- #content -->

<a href="#" class="topbutton"><img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/uparrow.png"></i></a>

<footer class="site-footer">
	<div class="le-clear container">
		<hr>
		<p class="copyright">Copyright <?php echo date('Y'); ?></p>
		<a href="https://liquidengine.com" target="_blank"><img class="le-logo" src="https://liquidengine.com/assets/le-logo-footer-dark.png" alt="Liquid Engine Websites" title="Liquid Engine websites"></a>
	</div>
</footer>

<?php get_template_part('template-parts/navbar/navbar', 'togglemenu' ); ?>

<?php get_sidebar( 'footer' ); ?>

<?php wp_footer(); ?>

<!-- Greensock -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js"></script>


<!-- scrollmagic -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
<!-- scrollmagic - debug - Remove for live -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>

</body>
</html>
