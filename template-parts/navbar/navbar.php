<nav class="navbar">
	<div class="hidden-xs hidden-sm">
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
	</div>
</nav>