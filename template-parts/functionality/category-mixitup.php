<!-- This uses the styling from gallery.scss -->
<!-- Create a custom post type and matching CUSTOM TAXONOMY -->

<div class="clearfix">
  <button class="filter" data-filter="all">All</button>
  <?php
  // Get all the team-tax taxonomies in use
  $args = array(
    'orderby' => 'name',
    'taxonomy'=> 'cat-team'
  );

  $filters = get_categories($args);

  foreach ($filters as $filter) {
    // Print out all the filters - each data-filter has to have the class including the full stop.
    // We'll prepend the categories with cat- to make it easier to ID later.
  	?>
    <button class="filter" data-filter="<?php echo '.cat-' . $filter->slug; ?>">
      <?php echo $filter->name; ?>
    </button>
  <?php } ?>

  <button class="sort" data-sort="my-order:asc">Ascending Order</button>
  <button class="sort" data-sort="my-order:desc">Descending Order</button>

  <div class="gallery gallery-border gallery-mixitup">		
    <?php

    $args2 = array(
      'posts_per_page' => -1,
      'post_type' => 'team',
      'orderby' => 'menu_order',
      'order' => 'ASC',
      'taxonomy' => 'cat-team'
      );

      $query = query_posts( $args2 );
      $index = 0;

      if (have_posts()) : while (have_posts()) : the_post();

      	$post = get_post();

        // Get every category from every taxonomy for this post
        $post_categories = wp_get_post_categories($post->ID);
        $catClasses = '';

        foreach ($post_categories as $c) {
          $cat = get_category( $c );
          // Create a single string of classes (including cat-) for each one.
          $catClasses = 'cat-' . $cat->slug . ' ' . $catClasses;
        }

        // Increase the index by 1 for ordering
        $index++;

        // Get the post for values such as title, permalink etc
        $item = get_post(get_the_ID());

      ?>
        <div class="gallery-item mix <?php echo $catClasses; ?>" data-my-order="<?php echo $index; ?>">
          <a href="<?php echo get_permalink($post->ID); ?>">
            <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
            <div class="overlay"></div>
            <div class="gallery-item-text"><?php echo $item->post_title; ?></div>
          </a>
        </div>
      <?php endwhile; else: ?>
          <p>There is nothing to see here</p>
      <?php
        endif;
        wp_reset_postdata();
        wp_reset_query();
      ?>
  </div>
</div>

<script>
    jQuery(function($){
        $('.gallery-mixitup').mixItUp();
    });	
</script>