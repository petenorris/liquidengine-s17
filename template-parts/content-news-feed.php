<?php
	// Requires a 'news' custom post type
	
  $posts = get_posts(array(
    'numberposts' => 1,
    'post_type' => 'news',
    'orderby' => 'menu_order',
    'order' => 'ASC'

  ));
  
  if($posts) {
    foreach($posts as $post) {
      setup_postdata( $post );
      ?>
      <div>
        <a  href="<?php echo get_permalink($post->ID) ?>"><h4 class="whatson-title"><?php the_title(); ?></h4></a>
        <?php the_date(); ?>
        <?php the_post_thumbnail(); ?>
        <?php the_excerpt(); ?>
        <?php the_content(); ?>
      </div>
      <?php
    }
  }
  wp_reset_postdata();
?>