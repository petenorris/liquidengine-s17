<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<div class="entry-content ">
		<?php the_content(); ?>
		<div class="pages row">
			<?php $pages = get_pages(array('child_of' => 29, 'sort_column' => 'menu_order')); ?>			
			<?php foreach ($pages as $page): ?>
				
        <a href="<?php echo get_permalink($page->ID) ?>" class="col-md-6">
        	<div class="img-frame">
        		<h2><?php echo $page->post_title; ?></h2>
        		<?php echo get_the_post_thumbnail($page->ID, 'full'); ?>
        		<div class="overlay">
        			
        		</div>
        		
        	</div>
       	</a>
			<?php endforeach; ?>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'liquidengine' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->