<!-- More info at https://developers.google.com/maps/documentation/javascript/ -->

<div id="map"></div>
    <script>
      function initMap() {
        var customMapType = new google.maps.StyledMapType([
            {
              // stylers: [
              //   {hue: '#0091d5'},
              //   {visibility: 'simplified'},
              //   {gamma: 0.5},
              //   {weight: 0.5}
              // ]
            },
            {
              // elementType: 'labels',
              // stylers: [{visibility: 'on'}]
            },
            {
              // featureType: 'water',
              // stylers: [
              // 	{color: '#0091d5'},
              // 	{visibility: 'simplified'},
              //   {gamma: 0.5},
              //   {weight: 0.1}
              // ]
            }
          ], {
            name: 'Custom Style'
        });
        var customMapTypeId = 'custom_style';
        var myLatLng = {lat: 39.490245, lng: 2.479027};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: myLatLng,  // Brooklyn.
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
          }
        });
        
        var marker = new google.maps.Marker({
			    map: map,
			    position: myLatLng,
			    title: 'Hello World!',
			    icon: '<?php bloginfo('template_directory'); ?>/dist/images/icons/map-pointer.png',
					animation: google.maps.Animation.DROP,
			  });

        map.mapTypes.set(customMapTypeId, customMapType);
        map.setMapTypeId(customMapTypeId);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB64hUh_oAKPx96KyyKCk-SxkhjnDA7NJE&callback=initMap">
    </script>



<style>

      #map {
        height: 300px;
      }
    </style>
