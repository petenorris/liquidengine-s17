<div class="gallery gallery-borer">
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lor Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
    
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>
  <div class="gallery-item">
    <a href="#">
        <img src="<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg">
        <div class="overlay"></div>
        <div class="gallery-item-text">Lorem Ipsum Dolor</div>
    </a>
  </div>  
</div>


<!-- PHP - Using ACF Gallery  -->
<!-- <div class="gallery">   
  <?php $images = get_field('gallery');
  if( $images ): ?>
    <?php foreach( $images as $image ): ?>
      <div class="gallery-item">
        <a href="<?php echo $image['url']; ?>">
          <img src="<?php echo $image['sizes']['mythumb300']; ?>" alt="<?php echo $image['alt']; ?>" />
          <div class="overlay"></div>
          <div class="gallery-item-text"><?php echo $image['caption']; ?></div>
        </a>
      </div>             
    <?php endforeach; ?>
  <?php endif; ?>
</div> -->