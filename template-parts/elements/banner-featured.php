<?php if (has_post_thumbnail()) {
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
	?>	
	<div class="banner parallax" data-enllax-ratio="0.5" style="background-image:url(<?php echo esc_url( $src[0] ); ?>);">
		<div class="container">
			<div class="banner-text fade-on-scroll">
				<h1>Main title</h1>
				<h2>Sub text</h2>
			</div>
		</div>
	</div>
	<?php
	}
else {
	?>
	<div class="banner parallax" data-enllax-ratio="0.5" style="background-color: <?php the_field('header_colour'); ?>;">
		<div class="container">
			<div class="banner-text fade-on-scroll">
				<h1>Main title</h1>
				<h2>Sub text</h2>
			</div>
		</div>
	</div>
	<?php
}
?>

<!-- HTML -->
<!-- <div class="banner parallax" data-enllax-ratio="0.5" style="background-image:url(<?php bloginfo('template_directory'); ?>/dist/images/slider/yachts.jpg);">
	<div class="container">
		<div class="banner-text fade-on-scroll">
			<h1>Main title</h1>
			<h2>Sub text</h2>
		</div>
	</div>
</div> -->