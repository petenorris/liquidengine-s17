<!-- REPEATER FIELD -->
<?php if( have_rows('image_slider') ): ?>
  <div class="myKenburns">
	  <div class="mySLick slider slider-full-screen slider-animate"><!-- slider-full-screen slider-animate -->
	    <?php while( have_rows('image_slider') ): the_row(); ?>
	 			<?php $image = get_sub_field('slider'); ?>
				<div class="slide parallax" data-enllax-ratio="0.5" style="background-image:url(<?php echo $image['url']; ?>);">
					<div class="container">
						<div class="banner-text fade-on-scroll" >
							<?php // $content = get_sub_field('content'); ?>
							<?php // $link = get_sub_field('link'); ?>
							<h1 class="animated bounceIn">Oooh! A slider!</h1>
							<h2>Sub text</h2>		
						</div>
					</div>
				</div>
	    <?php endwhile; ?>
	  </div>
	</div>
<?php endif; ?>