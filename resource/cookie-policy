<h3>Measuring our visitors</h3>
We measure visitors to our website using Google Analytics. This records what pages you view within our site, how you arrived at our site and some basic information about your computer. All of that information is anonymous – so we don’t know who you are; just that somebody visited our site.

The information we collect from analytics helps us understand what parts of our sites are doing well, how people arrive at our site and so on. Like most websites, we use this information to make our website better.

You can learn more about <a href="https://support.google.com/analytics/answer/6004245">Google Analytics</a> or <a href="https://tools.google.com/dlpage/gaoptout">opt out</a> if you wish.
<h3>Facebook, Twitter and other social networks</h3>
These services provide social buttons and similar features which we use on our website – such as the “Like” and “Tweet” buttons.

To do so we embed code that they provide and we do not control ourselves. To function their buttons generally know if you’re logged in; for example Facebook use this to say “x of your friends like this”. We do not have any access to that information, nor can we control how those networks use it.

Social networks therefore could know that you’re viewing this website, if you use their services (that isn’t to say they do, but their policies may change). As our website is remarkably inoffensive we imagine this is not a concern for most users.

<h3>If you sign up for a service</h3>
When you sign up for any of our services – paid or otherwise – we will record specific personal information about you, such as your name and email address.

We will also collect and store information about your use of our services so as to improve them. For example, we keep a log of what features are being used at any time.

We also log account and transaction history for accounting purposes, and to monitor our business activities.
<h3>Emails</h3>
We may send you email notifications regarding your service (such as invoices) or which you have specifically requested (such as newsletters or notifications when a report is completed). You have the ability to opt out of any of this communication at any time.

We will never provide your personal information or email address to any third parties except where they are specifically employed to help deliver our own services, as detailed above.
<h3>Online payment</h3>
We use a variety of payment providers to bill for our products online. These companies will have access to your personal and payment information. When paying by credit card, We do not ever have any access to your credit card details.

We share information with these companies only to the extent necessary for the purposes of processing payments you make via our website.

Updates to this policy

We may update this privacy policy from time-to-time, particularly as technology changes. You can always check this page for the latest version. We may also notify you of changes to our privacy policy by email.