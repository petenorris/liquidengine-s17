<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sandbox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{s}9-`5@I:m~c/W<Fi3Yp:X~1@/5tPH~l*-wRK_]v4;0q`1E<=.FM0fi}Zs;_k2W');
define('SECURE_AUTH_KEY',  'D#i5X6YHN(HJw&;AEF@@+nA}o>`bq;LDH:NHQ!Ww7l4W(zHsD`X/za3ssona}G)+');
define('LOGGED_IN_KEY',    'UOp._kemTYWAS}!|3|Iv/Lg-N/=2.Q0L(G(M@b:kC)S~7dHE],}mjf;=3d%OpX7s');
define('NONCE_KEY',        'x><+rwL2] 0cS4Y0an5mk8.k&E=-CO^T-:<j(kfXN3iOMmR8wjEx/5=KdcgYo~Hu');
define('AUTH_SALT',        'hm1q2{cq22)}@T8{#*BT0b*0l-pOQun[v^q-JeMzi26.fYmD-oCiQ{jyzQ_u$iIc');
define('SECURE_AUTH_SALT', '}kgOB9UP$^} %.dS:RS&a@`ELq)0~gY9BiuE{(VKa>uNilM+SDV,#k3nCw}RO1FB');
define('LOGGED_IN_SALT',   'Zn]+.PbFpM^`n%LC/^p0|tmV|xn,l9rlnd-&pxs75^D`o`Ana-1;lo^*MDCAcOmY');
define('NONCE_SALT',       's%45Dd&p~PEaCfu|fX|/-<wsdno>08{#Nfh:iB#?<s9%zp_a.)1M8q9E1.vC-,0}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */


define('WP_DEBUG', false);
define('WP_SITEURL', 'http://sandbox.local/');
define('WP_HOME', 'http://sandbox.local/');
/** Disable editor */
define('DISALLOW_FILE_EDIT',true);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
