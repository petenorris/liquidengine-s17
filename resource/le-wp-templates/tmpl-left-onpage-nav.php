<?php
/*
Template Name: Left onpage nav
*/
?>

<!--
1. CREATE ACF REPEATER FIELD
2. Sub field - section_title
3. Sub field - section_content
-->


<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="sidebar">
				<ul>
					<?php
					if( have_rows('section') ):
					  while ( have_rows('section') ) : the_row();
					  ?>
				      <li><a href="#<?php the_sub_field('section_title');?>"><?php the_sub_field('section_title');?></a></li>
				    <?php
				    endwhile;
				   	else :
				    	?><p>It looks like you have no slides :(</p><?php
				  endif;
					?>
				</ul>	    
				<?php get_sidebar(); ?>
			</div>
		</div>
		<div id="primary" class="col-md-6 content-area">
			<main id="main" class="site-main">
				<?php
				if( have_rows('section') ):
				  while ( have_rows('section') ) : the_row();
				  	?>
			    	<div id="<?php the_sub_field('section_title');?>" class="clearfix">
			      	<h2><?php the_sub_field('section_title');?></h1>
			      	<?php the_sub_field('section_content');?>
			    	</div>
			    <?php
			    endwhile;
			    else :
			  	  ?><p>It looks like you have no slides :(</p><?php
			  endif;
			  ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- row -->
</div><!-- container -->	

<?php get_footer(); ?>
