// LE Scripts
// Navbar

jQuery(document).ready(function($) {

	//SIDR
  // $('.sidr-toggle').sidr( {
  // 	// name: 'sidr-right',
  //  //  side: 'right'
  // });

  //

  //TOGGLEMENU
  var toggleMenu = function() {
	  $('nav.nav--toggle').toggleClass('toggle');
	  // $('body').toggleClass('push');
	  $('.toggle-overlay').toggleClass('block');
	  $('#social, .logo').toggleClass('reveal');
  };

	//Nav
	$('.toggle').click(function() {
    toggleMenu();
	});

  // Mousetrap.bind('esc', function() {
  //   toggleMenu();
  // });



	// headroom
	$(".headroom").headroom({
	  "offset": 205,
	  "tolerance": 5,
	  // "classes": {
	  //   "initial": "animated",
	  //   "pinned": "slideDown",
	  //   "unpinned": "slideUp"
	  // }
	});

	// SLick SLider
	$('.slider').slick({
	  arrows: false,
	  dots: false,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  cssEase: 'ease-in-out',
    lazyLoad: 'progressive',
	  autoplaySpeed: 7000,
	  autoplay: true
	});

	// fade on scroll
	$(window).scroll(function(){
    $(".fade-on-scroll").css("opacity", 1 - $(window).scrollTop() / 500);
  });

	// Banner animated
  $(".bg-pan").css("background-size", 1 - $(window).scrollTop() / 900);
  
  // Parallax-image
  $('.parallax').enllax();
  
  // Scroll reveal
  var fadeup = {
	  delay    : 100,
	  distance : '90px',
	  easing   : 'ease-in-out'
	};
	window.sr = ScrollReveal();
	sr.reveal('.sr', fadeup);

	// Masonry
	$('.grid').masonry({
	  itemSelector: '.grid-item',
	  columnWidth: 160
	});	

	// Back to top button
  var offset = 600;
  var speed = 250;
  var duration = 1000;
   $(window).scroll(function(){
          if ($(this).scrollTop() < offset) {
		     $('.topbutton') .fadeOut(duration);
          } else {
		     $('.topbutton') .fadeIn(duration);
          }
      });
	$('.topbutton').on('click', function(){
		$('html, body').animate({scrollTop:0}, speed);
		return false;
	});

});