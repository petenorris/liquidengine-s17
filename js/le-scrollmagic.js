// LE Scripts
jQuery(document).ready(function($) {

	// Init scrollmagic
	var controller = new ScrollMagic.Controller();
	

	///////////////////
	/// PIN ELEMENT ///
	///////////////////
	// Pin - release - pin (ie. intro)
	// Part 1 - Initial pinning
	var pinIntroScene = new ScrollMagic.Scene({
		triggerElement:'.card-intro',
		triggerHook:0.1,
		duration:100
	})
	.setPin('.card-intro', {
		pushFollowers: false
	})
	.addIndicators({
		name: 'intro scene',
		colorTrigger: 'black',
		colorStart:'#75c695',
		colorEnd: 'red'
	}) //Adds indicators for debugging - need to REMOVE
	.addTo(controller);
	// Part 2 - second 'pinning'
	var pinIntroScene2 = new ScrollMagic.Scene({
		triggerElement:'.scroll-fade-in',
		triggerHook:0.4,
		duration:'20%'
	})
	.setPin('.card-intro', {
		pushFollowers: false
	})
	// .addIndicators({
	// 	name: 'intro scene 2',
	// 	colorTrigger: 'black',
	// 	colorStart:'#75c695',
	// 	colorEnd: 'red',
	// 	indent:200
	// }) //Adds indicators for debugging - need to REMOVE
	.addTo(controller);

	
	////////////////
	/// PARALLAX ///
	////////////////
	var parallaxTl = new TimelineMax();
	parallaxTl
		.from('.parallax-content', 0.2, {autoAlpha: 0, ease:Power0.easeNone}, 0.4)
		.from('.parallax-bg', 2, {y: '-50%', ease:Power0.easeNone}, 0)
		;
	var parallaxBg = new ScrollMagic.Scene({
		triggerElement:'.parallax',
		triggerHook:1,
		duration:'200%'
	})
	.setTween(parallaxTl)
	.addTo(controller);


	///////////////
	/// FADE-IN ///
	///////////////
	// loop through each .project element
	$('.card-scroll').each(function() {
		// Build a scene
		var ourScene = new ScrollMagic.Scene( {
			triggerElement: this, //THis is the trigger that initiates the scene (children[0] value from console log)
			// duration: '90%',
			// duration: 100% Good for responsive (or 90% for dissapearing just before it leaves top of screen)
			triggerHook:0.9, // 0 for top of viewport, 1 for bottom
			// reverse: false // prevents effect happenning again - doesn't work with duration
		})
		.setClassToggle(this, 'fade-in') // add calls to scene
		// .addIndicators({
		// 	name: 'fade scene',
		// 	colorTrigger: 'black',
		// 	colorStart:'#75c695',
		// 	colorEnd: 'red'
		// }) //Adds indicators for debugging - need to REMOVE
		.addTo(controller);
	});


	///////////////////////
	/// GREENSOCK TWEEN ///
	///////////////////////
	var header = document.getElementById('header'),
		h1 = document.getElementsByTagName('h1'),
		intro = document.getElementsByClassName('intro');
		firstItem = document.getElementsByClassName('list')[0].firstElementChild;
		secondItem = document.getElementsByClassName('list')[0].children[1];
		lastItem = document.getElementsByClassName('list')[0].lastElementChild;

	TweenLite.to(firstItem, 3, {opacity: 0, x:200}); //item, timing {css}
	TweenLite.to(secondItem, 10, {opacity: 0, x:200}); //item, timing {css}
	TweenLite.to(lastItem, 20, {opacity: 0, x:200}); //item, timing {css}
	

});

/////////////
/// TWEEN ///
/////////////
